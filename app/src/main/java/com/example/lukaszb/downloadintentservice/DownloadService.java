package com.example.lukaszb.downloadintentservice;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class DownloadService extends IntentService {
    private String path,fileName;
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_DOWNLOAD = "com.example.lukaszb.downloadintentservice.action.DOWNLOAD";
    public static final String ACTION_FINISHED = "com.example.lukaszb.downloadintentservice.action.FINISHED";
    public static final String ACTION_PROGRESS = "com.example.lukaszb.downloadintentservice.action.PROGRESS";

    private static final String EXTRA_PARAM1 = "com.example.lukaszb.downloadintentservice.extra.PARAM1";

    public DownloadService() {
        super("DownloadService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionDownload(Context context, String param1) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_PARAM1, param1);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                handleActionDownload(param1);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionDownload(String param1) {
        path=param1;
        fileName=extractFileName();
        long startTime=0,stopTime=0;
        String localPath=getLocalPath();
        int totalSize=0;
        try {
            URL url = new URL(path);
            HttpURLConnection connection=(HttpURLConnection) url.openConnection();
            totalSize=connection.getContentLength();
            try(InputStream is=connection.getInputStream();
                FileOutputStream fos=new FileOutputStream(localPath+"/"+fileName)){
                long currentSize=0;
                int bufSize=2048,c;
                startTime=System.currentTimeMillis();
                byte[] buffer=new byte[bufSize];
                while((c=is.read(buffer,0,bufSize))>-1){
                    fos.write(buffer,0,c);
                    currentSize+=c;
                    int procent=(int)(((double)currentSize)/((double)totalSize)*100);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast((new Intent(ACTION_PROGRESS).putExtra("PERCENT",procent)));
                }
                stopTime=System.currentTimeMillis();
            }
        }catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent in=new Intent(ACTION_FINISHED).putExtra("TIME",(int)(stopTime-startTime)/1000);
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }

    private String getLocalPath(){
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath().toString();
    }

    private String extractFileName(){
        return path.substring(path.lastIndexOf("/")+1) ;
    }

}
