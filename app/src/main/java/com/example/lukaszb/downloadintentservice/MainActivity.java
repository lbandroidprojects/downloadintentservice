package com.example.lukaszb.downloadintentservice;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button bDownload;
    private ProgressBar progressBar;
    private final static int EXTERNAL_STORAGE_PERMISSION = 17;
    private final static String DOWNLOADURL = "http://alex.smola.org/drafts/thebook.pdf";
    private IntentFilter ifFinished;
    private FinishedReceiver fr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bDownload = findViewById(R.id.bDownload);
        progressBar = findViewById(R.id.progressBar);

        bDownload.setOnClickListener(this);

        ifFinished = new IntentFilter();
        ifFinished.addAction(DownloadService.ACTION_FINISHED);
        ifFinished.addAction(DownloadService.ACTION_PROGRESS);
        fr = new FinishedReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(fr, ifFinished);
    }

    private boolean isNetworkActive() {
        boolean active = false;
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        active = ni != null && ni.isAvailable() && ni.isConnected();
        return active;

    }

    @Override
    public void onClick(View v) {
        if (isNetworkActive()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                DownloadService.startActionDownload(this, DOWNLOADURL);
            } else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION);
        } else {
            Toast.makeText(this, "Brak połączenia sieciowego", Toast.LENGTH_SHORT).show();
        }
    }

    class FinishedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case DownloadService.ACTION_PROGRESS:
                    int procent=intent.getIntExtra("PERCENT",0);
                    progressBar.setProgress(procent);
                    break;
                case DownloadService.ACTION_FINISHED:
                    int time=intent.getIntExtra("TIME",0);
                    Toast.makeText(context, "Plik załadowany po "+time+" sekundach", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
